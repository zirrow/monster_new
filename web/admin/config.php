<?php
// HTTP
define('HTTP_SERVER', 'http://dev.monsterhighshop.com.ua/admin/');
define('HTTP_CATALOG', 'http://dev.monsterhighshop.com.ua/');

// HTTPS
define('HTTPS_SERVER', 'http://dev.monsterhighshop.com.ua/admin/');
define('HTTPS_CATALOG', 'http://dev.monsterhighshop.com.ua/');

// DIR
define('DIR_APPLICATION', '/home/vorobey/monsterhighshop.com.ua/dev/web/admin/');
define('DIR_SYSTEM', '/home/vorobey/monsterhighshop.com.ua/dev/web/system/');
define('DIR_IMAGE', '/home/vorobey/monsterhighshop.com.ua/dev/web/image/');
define('DIR_STORAGE', '/home/vorobey/monsterhighshop.com.ua/dev/storage/');
define('DIR_CATALOG', '/home/vorobey/monsterhighshop.com.ua/dev/web/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'vorobey.mysql.tools');
define('DB_USERNAME', 'vorobey_monstern');
define('DB_PASSWORD', 'lxmqnjc3');
define('DB_DATABASE', 'vorobey_monstern');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
